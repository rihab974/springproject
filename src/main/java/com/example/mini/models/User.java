package com.example.mini.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    private String code;

    @NotNull
    @Size(max = 50)
    private String nom;

    @NotNull
    @Size(max = 50)
    private String prenom;

    @NotNull
    private Date dateEmbauchement;

    @NotNull
    @Size(max = 50)
    private String login;

    @NotNull
    @Size(min = 6, max = 10)
    private String password;
    @OneToMany(mappedBy = "utilisateur", cascade = CascadeType.ALL)
    private Set<Conge> conges = new HashSet<>();
    public User() {
    }

    public User(Long id, String code, String nom, String prenom, Date dateEmbauchement, String login, String password) {
        this.id = id;
        this.code = code;
        this.nom = nom;
        this.prenom = prenom;
        this.dateEmbauchement = dateEmbauchement;
        this.login = login;
        this.password = password;

    }
}